//7.-Encontrar número de vocales de una cadena

object cadenaVocales {
  
  def obtenerVocales(cadena: String, n:Int): Int={
    
   if(n<0)
   {
     0
   }
   else
   {
     if(cadena.charAt(n).equals('a')||cadena.charAt(n).equals('e')||cadena.charAt(n).equals('i')||cadena.charAt(n).equals('o')||cadena.charAt(n).equals('u'))
     {
       //print("vocales")
       return 1+obtenerVocales(cadena,n-1)
       
     }else
     {
       return obtenerVocales(cadena,n-1)
     }
   
   }
  }
  
  def main(args: Array[String]): Unit = {
    val cadena="hola"
    
    print(obtenerVocales(cadena,(cadena.length()-1)))
  }

  
}