

object sumatoriaLimites {
  
   def sumaEnterosConLimitesRecursiva(li: Int, ls:Int): Int={
   
    if(ls<li)
      0
    else
      return ls+sumaEnterosConLimitesRecursiva(li,ls-1)
  }
   
  def main(args: Array[String]): Unit = {
    
    println(sumaEnterosConLimitesRecursiva(5,10))
  }
}