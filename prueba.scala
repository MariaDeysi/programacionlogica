import scala.collection.mutable.ArrayBuffer;

class Alumno(  private var _noControl: String="ND", 
               private var _nombre: String="ND", 
               private var _primerAp: String= "ND",
               private var _segundoAp: String= "ND",
               private var _edad: Byte= 0,
               private var _semestre: Byte= 0,
               private var _carrera: String="ND",
               private var _fechaN: String="ND",
               private val _calificaciones:List[List[Int]]){
  
  
  def noControl = _noControl //getter

  def noControl_= (noControl: String): Unit={ //setters
    _noControl = noControl
  }
  
  
  def nombre = _nombre

  def nombre_= (nombre: String): Unit={
    _nombre = nombre
  }
  
  def primerAp = _primerAp

  def primerAp_= (primerAp: String): Unit={
    _primerAp = primerAp
  }
  
    
   def segundoAp = _segundoAp //getter

  def segundoAp_= (segundoAp: String): Unit={ //setters
    _segundoAp = segundoAp
  }
  
     
   def edad = _edad //getter

  def edad_= (edad: Byte): Unit={ //setters
    _edad = edad
  }
   
  def semestre = _semestre //getter

  def semestre_= (semestre: Byte): Unit={ //setters
    _semestre = semestre
  }
  
   def carrera = _carrera //getter

  def carrera_= (carrera: String): Unit={ //setters
    _carrera = carrera
  }
   
   def fechaN = _fechaN //getter

  def fechaN_= (fechaN: String): Unit={ //setters
    _fechaN = fechaN
  }
  
   def calificaciones = _calificaciones //getter

  def calificaciones_= (calificaciones: List[List[Int]]): Unit= { //setters
    val _calificaciones = calificaciones 
    
   }
  
}
object prueba {
  
  def main(args: Array[String]): Unit = {
    
     var promedioGrupal=0.0
    
     var noControl="14070080" 
     var nombre="Julio"
     var pAp="Hernandez"
     var sAp="Casas"
     var edad=30.toByte
     var semestre=6.toByte
     var carrera="ISC"
     var fechaN="93/03/25"
     
   
     val calificaciones1=List(
                   List(80, 90, 67, 110,98,70),   //Semestre
                    List(80, 90, 100, 110,98,70), 
                    List(80, 68, 100, 110,98,70), 
                    List( 80, 90, 100, 50,98,70),
                    List(69, 90, 100, 110,98,70))
                    
    var noControl2="14070080" 
     var nombre2="Julio"
     var pAp2="Hernandez"
     var sAp2="Casas"
     var edad2=30.toByte
     var semestre2=6.toByte
     var carrera2="ISC"
     var fechaN2="93/03/25"
     
   
     val calificaciones2=List(
                   List(50, 90, 100, 110,60,70),   //Semestre
                    List(80, 90, 100, 110,98,70), 
                    List(80, 90, 100, 50,98,70), 
                    List( 80, 45, 100, 110,98,70),
                    List(80, 90, 100, 110,98,70))
                    
     var noControl3="14070080" 
     var nombre3="Julio"
     var pAp3="Hernandez"
     var sAp3="Casas"
     var edad3=30.toByte
     var semestre3=6.toByte
     var carrera3="ISC"
     var fechaN3="93/03/25"
     
   
     val calificaciones3=List(
                   List(80, 90, 100, 110,98,70),   //Semestre
                    List(80, 50, 100, 110,98,70), 
                    List(80, 90, 100, 67,98,70), 
                    List( 80, 90, 100, 110,98,70),
                    List(80, 50, 100, 110,98,70))
                    
     var noControl5="14070080" 
     var nombre5="Julio"
     var pAp5="Hernandez"
     var sAp5="Casas"
     var edad5=30.toByte
     var semestre5=6.toByte
     var carrera5="ISC"
     var fechaN5="93/03/25"
     
   
     val calificaciones5=List(
                   List(80, 90, 45, 110,98,70),   //Semestre
                    List(80, 90, 100, 70,98,70), 
                    List(80, 90, 100, 110,98,67), 
                    List( 80, 90, 50, 110,98,70),
                    List(80, 90, 100, 110,98,70))
                    
     var noControl4="14070080" 
     var nombre4="Julio"
     var pAp4="Hernandez"
     var sAp4="Casas"
     var edad4=30.toByte
     var semestre4=6.toByte
     var carrera4="ISC"
     var fechaN4="93/03/25"
     
   
     val calificaciones4=List(
                   List(80, 90, 100, 67,98,70),   //Semestre
                    List(80, 90, 60, 110,98,70), 
                    List(80, 90, 100, 50,98,67), 
                    List( 80, 90, 100, 110,98,70),
                    List(80, 90, 100, 67,98,70))
                    
                    

     val a1 = new Alumno(noControl, nombre, pAp, sAp, edad, semestre, carrera, fechaN, calificaciones1)
     val a2 = new Alumno(noControl2, nombre2, pAp2, sAp2, edad2, semestre2, carrera2, fechaN2, calificaciones2)
     val a3 = new Alumno(noControl3, nombre3, pAp3, sAp3, edad3, semestre3, carrera3, fechaN3, calificaciones3)
     val a4 = new Alumno(noControl4, nombre4, pAp4, sAp4, edad4, semestre4, carrera4, fechaN4, calificaciones4)
     val a5 = new Alumno(noControl5, nombre5, pAp5, sAp5, edad5, semestre5, carrera5, fechaN5, calificaciones5)
     
     println("Datos de alumno 1-------------------")
     println("Promedio Alumno1: " + obtenerPromedioAlumno(a1.calificaciones))
     obtenerRFC(a1)
     obtenerCalificacioneAp(calificaciones1)
     println()
     println("Datos de alumno 2-------------------")
     println("Promedio Alumno: " + obtenerPromedioAlumno(a2.calificaciones))
     obtenerRFC(a2)
     obtenerCalificacioneAp(calificaciones2)
     println()
     println("Datos de alumno 3-------------------")
     println("Promedio Alumno: " + obtenerPromedioAlumno(a3.calificaciones))
     obtenerRFC(a3)
     obtenerCalificacioneAp(calificaciones3)
     println()
     println("Datos de alumno 4-------------------")
     println("Promedio Alumno: " + obtenerPromedioAlumno(a4.calificaciones))
     obtenerRFC(a4)
     obtenerCalificacioneAp(calificaciones4)
     println()
     println("Datos de alumno 5-------------------")
     println("Promedio Alumno: " + obtenerPromedioAlumno(a5.calificaciones))
     obtenerRFC(a5)
     obtenerCalificacioneAp(calificaciones5)
     println()
     promedioGrupal=(obtenerPromedioAlumno(a1.calificaciones)+
                     obtenerPromedioAlumno(a2.calificaciones)+
                     obtenerPromedioAlumno(a3.calificaciones)+
                     obtenerPromedioAlumno(a4.calificaciones)+
                     obtenerPromedioAlumno(a5.calificaciones))/5
                     
    println()  
    println("Datos generales----------------------------------------")
    
     println("Promedio Grupal: "+ promedioGrupal)
     
     estadisticaSemestresRep(calificaciones1,calificaciones2,calificaciones3,calificaciones4,calificaciones5)
    
  }
  
  def obtenerPromedioAlumno(calificaciones: List[List[Int]]): Double={
    
    var promedioAlumno=0.0
    val tamaño=0.toInt
    
    for(e<- calificaciones){
      
      for(c<- e){
        
        promedioAlumno=promedioAlumno+c    
        
   
      }
      
     
    }
    
    promedioAlumno=(promedioAlumno/(calificaciones.length*6))
    
    promedioAlumno.toDouble
    
    //println("Promedio Alumno1: "+promedioAlumno)
  }
  
  def obtenerRFC(alumno: Alumno): Unit={
    
    var rfc=""
    var letra=""
    var contador=0
    
    rfc=alumno.primerAp.charAt(0).toString();
    
    for(i <- 0 until alumno.primerAp.length){
        
       letra=alumno.primerAp.charAt(i).toString()
       
       if(contador<1){
         if(letra.equals("a")||letra.equals("e")||letra.equals("i")||letra.equals("o")||letra.equals("u")){
       
             letra=letra.toUpperCase()
             rfc=rfc+letra
             contador+=1
         }
       }
    }
       
       
     rfc=rfc+alumno.segundoAp.charAt(0).toString();
     rfc=rfc+alumno.nombre.charAt(0).toString();
     
     for(i <- 0 until alumno.fechaN.length){
        
       letra=alumno.fechaN.charAt(i).toString()
       
     
         if(letra.equals("/")){

         }else
           rfc=rfc+letra
    }
     //rfc=rfc+alumno.fechaN;
     
    println(rfc)
    
    
  }
  
 
 /*La letra inicial y la primera vocal interna del primer apellido, 
  * la letra inicial del segundo apellido y la primera letra del nombre.
  * La fecha de nacimiento en el orden de año mes y día. 
  * Para el año se tomarán los dos últimos dígitos, cuando el mes o el día sea menor a diez, se antepondrá un cero. 
  */
  
  def obtenerCalificacioneAp(calificaciones: List[List[Int]]): Unit={
    
     val calAp=ArrayBuffer[Int]()
     var contador=0
     var contador2=0
     var contador3=0
     
     //calAp+=1
     
    for(e<- calificaciones){
      contador+=1
      contador3=0
      
      for(c<- e){
        
        if(c >= 70){
          
          if(contador3==0){
            
            calAp+=contador
            
          }
          
          calAp+=c
        }
        
        contador3+=1
   
      }
     
      }
    
     println()
    println("Calificaciones aprobatorias")
    
    
     for(e<- calAp){
       contador2+=1
       
       if(e<=69){
         
         println()
       }
       
       print(e+" ")
     }
     
      println()
    
  }
  
  def estadisticaSemestresRep(c1: List[List[Int]], c2: List[List[Int]],
                              c3: List[List[Int]], c4: List[List[Int]],
                              c5: List[List[Int]] ): Unit={
    
    var s1=0
    var s2=0
    var s3=0
    var s4=0
    var s5=0
    var s1r=0
    var s2r=0
    
    var contador=0
    var noSemestre=0
    val reprobadas1= estadisticaSemestresRep2(c1)
    val reprobadas2= estadisticaSemestresRep2(c2)
    val reprobadas3= estadisticaSemestresRep2(c3)
    val reprobadas4= estadisticaSemestresRep2(c4)
    val reprobadas5= estadisticaSemestresRep2(c5)
    
    s1=reprobadas1(0)+reprobadas2(0)+reprobadas3(0)+reprobadas4(0)+reprobadas5(0)
    s2=reprobadas1(1)+reprobadas2(1)+reprobadas3(1)+reprobadas4(1)+reprobadas5(1)
    s3=reprobadas1(2)+reprobadas2(2)+reprobadas3(2)+reprobadas4(2)+reprobadas5(2)
    s4=reprobadas1(3)+reprobadas2(3)+reprobadas3(3)+reprobadas4(3)+reprobadas5(3)
    s5=reprobadas1(4)+reprobadas2(4)+reprobadas3(4)+reprobadas4(4)+reprobadas5(4)

    //println("Indice de reprobacion en cada semestre")
    
    
     val reprobadas= Array(s1,s2,s3,s4,s5)
     
     var contador2=0
     
     
     for(e<-reprobadas){
       
       
       
        if(contador2<(reprobadas.length-1)){
         
          if(contador2==0)
                s1r=e
                
          if(s1r<=reprobadas(contador2+1)){
         
           s1r=reprobadas(contador2+1)
           noSemestre=contador2
       
          }else{
         
           s1r=s1r
           noSemestre=contador2
         }
       }
        
        contador2+=1
         
     }
     
    println()
    println("Semestre con mayor reprobacion: ")
    println("No de semestre "+ noSemestre+ ": " + s1r)
   // println(s1)
    
  }
  
  def estadisticaSemestresRep2(c1: List[List[Int]]): Array[Int] ={
    
   
    var s1=0
    var s2=0
    var s3=0
    var s4=0
    var s5=0
    var contador=0
    
    for(e<- c1){
      contador+=1
      //contador3=0
      
      for(c<- e){
        
        if(c <= 69){
          
          if(contador==1){
            s1+=1
          }else if(contador==2){
            s2+=1
          }
          else if(contador==3){
            s3+=1
          }
          else if(contador==4){
            s4+=1
          }
          else if(contador==5){
            s5+=1
          }
          
        }

   
      }
     
      }
    
     val reprobadas= Array(s1,s2,s3,s4,s5)
     
     reprobadas
    
  }
  

  
}

