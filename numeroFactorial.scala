//3.-Obtener numero factorial

object numeroFactorial {
  
  def numeroFactorial(n:Int): Int={
    
    if(n==1)
      1
    else
      return n*numeroFactorial(n-1)
  }
  
  def main(args: Array[String]): Unit = {
    
    print(numeroFactorial(5))
    
  }
}