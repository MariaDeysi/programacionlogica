//5.-No de divisores entre dos numeros dados

object noDivisores {
  
  def obtenerNoDivisores(n1: Int, n2:Int, n3:Int): Int={
    
    if(n3==n2)
      0
    else{
      
      if(n1%n3==0){
        
        return 1+obtenerNoDivisores(n1,n2,(n3-1))
        
      }else
        
        return obtenerNoDivisores(n1,n2,(n3-1))
    }
  }
  
  def main(args: Array[String]): Unit = {
    
    val n1=40
    val n2=5
    
    print(obtenerNoDivisores(n1,n2,n1))
    
  }
  
}